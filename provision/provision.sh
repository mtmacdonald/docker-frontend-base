#!/bin/bash
# ------------------------------------------------------------------------------
# Provisioning script for the docker-frontend-base image
# ------------------------------------------------------------------------------

apt-get update && apt-get upgrade -y -o Dpkg::Options::="--force-confold"

# ------------------------------------------------------------------------------
# Locales
# ------------------------------------------------------------------------------

apt-get install -y locales

# ------------------------------------------------------------------------------
# Curl
# ------------------------------------------------------------------------------

apt-get -y install curl

# ------------------------------------------------------------------------------
# Supervisor
# ------------------------------------------------------------------------------

# install python (required for Supervisor)
apt-get -y install python3 python3-pip

# directories and conf files
mkdir -p /etc/supervisord/
mkdir /var/log/supervisor
cp /provision/conf/supervisor.conf /etc/supervisord.conf

# install
pip3 install supervisor

# ------------------------------------------------------------------------------
# Cron
# ------------------------------------------------------------------------------

apt-get -y install cron
cp /provision/service/cron.conf /etc/supervisord/cron.conf

# ------------------------------------------------------------------------------
# Nano
# ------------------------------------------------------------------------------

apt-get -y install nano

# Fix 'Error opening terminal: unknown' in docker exec for older Docker versions
# askubuntu.com/questions/736101
# https://github.com/docker/docker/issues/9299
# http://stackoverflow.com/questions/27826241
echo 'export TERM=xterm' >> /etc/bash.bashrc

# ------------------------------------------------------------------------------
# Zip and unzip
# ------------------------------------------------------------------------------

apt-get -y install zip unzip

# ------------------------------------------------------------------------------
# Git version control
# ------------------------------------------------------------------------------

apt-get -y install git

# ------------------------------------------------------------------------------
# Unattended upgrades (security patches)
# ------------------------------------------------------------------------------

apt-get -y install unattended-upgrades
dpkg-reconfigure unattended-upgrades

# ------------------------------------------------------------------------------
# NGINX web server
# ------------------------------------------------------------------------------

apt-get -y install nginx
cp /provision/service/nginx.conf /etc/supervisord/nginx.conf

#configuration files
cp /provision/conf/nginx/development /etc/nginx/sites-available/default
cp /provision/conf/nginx/production.template /etc/nginx/sites-available/production.template

# disable 'daemonize' in nginx (because we use supervisor instead)
echo "daemon off;" >> /etc/nginx/nginx.conf

# ------------------------------------------------------------------------------
# Node 12 LTS
# ------------------------------------------------------------------------------

curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
apt-get -y install build-essential # for binary npm packages

# ------------------------------------------------------------------------------
# Clean up
# ------------------------------------------------------------------------------
rm -rf /provision
